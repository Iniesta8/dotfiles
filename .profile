export TERMINAL=/usr/bin/alacritty
export EDITOR=/usr/bin/nvim
# fix "xdg-open fork-bomb" export your preferred browser from here
export BROWSER=/usr/bin/firefox

