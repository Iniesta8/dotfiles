# dotfiles

Some of my dotfiles that I use on linux machines. Buyer beware ;)

## Setup

### Manual install

Just create symlinks between cloned repository and your home directory...


## Some important neovim shortcuts

| Shortcut | Description |
| -------- | ----------- |
| `,`        | Leader |
| `,w`       | Fast saving |
| `,W`       | Sudo write |
| `,q`       | Quick close buffer |
| `,Q`       | Quick close window |
| `,,`       | Open last buffer |
| `,b`       | List buffers |
| `gb`       | Next buffer |
| `gB`       | Previous buffer |
| `Y`        | Yank from cursor to end of line |
| `J`        | Join lines and restore cursor location |
| `Space`    | Search |
| `Ctrl+Space` | Backwards search |
| `Ctrl+p`   | Fuzzy search files |
| `,rg`      | Fuzzy grep file content |
| `,*`       | Replace |
| `,[1-6]`   | Highlight interesting word |
| `,nn`      | Toggle NERDTree |
| `,n`       | Focus NERDTree |
| `,C`       | Toggle auto clang-format |
| `,N`       | Toggle relative line numbers |
| `,fc`      | Find merge conflict markers |
| `,ff`      | Display all lines with keyword under cursor and ask which one to jump to |
| `gd`       | Goto definition |
| `gi`       | Goto implementation |
| `,rn`      | Rename symbol under cursor | 

